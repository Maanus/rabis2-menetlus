import InputNumber from '@/components/InputTypes/InputNumber'
import InputDate from '@/components/InputTypes/InputDate'
import InputText from '@/components/InputTypes/InputText'
import InputTextarea from '@/components/InputTypes/InputTextarea'
import InputFile from '@/components/InputTypes/InputFile'

export default function (formItem) {
  const type = formItem.type
  switch (type) {
    case 'number':
      return InputNumber
    case 'string':
      return InputText
    case 'date':
      return InputDate
    case 'textbox':
      return InputTextarea
    case 'file':
      return InputFile
    default:
      console.error(`FormElementMapper: missing handle case for ${type}`)
  }
}
