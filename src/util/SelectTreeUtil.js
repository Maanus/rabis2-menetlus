export function updateSelectTree(array, replaceableItem) {
  return Array.isArray(array) && array.some((item, i, _array) => {
    if (item.id === replaceableItem.id) {
      _array[i] = replaceableItem;
      return true;
    }
    return updateSelectTree(item.children, replaceableItem);
  });
}
