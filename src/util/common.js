export default function extractAttributes(Attributes) {
  Attributes = Attributes.Attribute || Attributes || [];
  return Attributes.reduce((result, attribute) => {
    result[attribute.Name] = attribute.Value
    return result
  }, {})
}
