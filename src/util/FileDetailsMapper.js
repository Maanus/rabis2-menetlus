import { DetailFormItem, DetailFormFileItem } from '@/model/FormItem.js'
import extractAttributes from './common.js'

export default (rawDetails) => {
  const { Id, Name, Attributes } = rawDetails
  const attributes = extractAttributes(Attributes)
  const filteredFields = {
    id:                 Id,
    name:               Name,
    attributes
  }

  switch (attributes.type) {
    case 'file':
      return new DetailFormFileItem(filteredFields)
    default:
      return new DetailFormItem(filteredFields)
  }
}
