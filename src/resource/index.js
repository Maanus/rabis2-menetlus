import vue from 'vue'
import cloneDeep from 'lodash/cloneDeep';
import { URL_BASE, ROOT_ELEMENTS } from '@/config.js'

export default {

  fetchRootTreeItems(commit) {
    // simulate multiple roots
    const treeRootResourcePromises = [ROOT_ELEMENTS, cloneDeep(ROOT_ELEMENTS[0])].map(rootElement => {
      return vue.http.get(`${URL_BASE}/tree-select/${rootElement}.json`)
      .then(res => res.json())
      .catch(e => {
        if (isLostConnectionException(e)) {
          commit({type: "CONNECTION_LOST"})
        } else {
          throw e
        }
      })
    });
    return Promise.all(treeRootResourcePromises)
  },
  fetchTreeItems(commit, treeItem) {
    try {
      return vue.http.get(`${URL_BASE}/tree-select/${treeItem.name}.json`)
    } catch(e) {
      if (isLostConnectionException(e)) {
        commit({type: "CONNECTION_LOST"})
      } else {
        throw e
      }
    }
  }
}

const isLostConnectionException = (e) => e.status === 0
