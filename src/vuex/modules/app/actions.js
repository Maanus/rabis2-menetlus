import {
  START_LOADING,
  STOP_LOADING
} from './mutation-type'

export const changeAppLoadingState = ({commit}, isLoading) => {
  if (isLoading) {
    commit(FINISH_LOADING)
  } else {
    commit(START_LOADING)
  }
}
