import {
  START_LOADING,
  STOP_LOADING,
  CONNECTION_LOST
} from './mutation-type'

const mutations = {
  [START_LOADING] (state) {
    state.loading = true
  },
  [STOP_LOADING] (state) {
    state.loading = false
  },
  [CONNECTION_LOST] (state) {
    console.log("connection lost")
  }
}

export default mutations
