import { getActiveFileFields } from './util.js'
import cloneDeep from 'lodash/cloneDeep';
import {
  FETCH_FILE_DETAIL_SUCCESS,
  ACTIVE_FILE_DETAIL,
  UPDATE_FORM_FIELD,
  UPDATE_FORM_FILES,
} from './mutation-type'

const mutations = {
  [FETCH_FILE_DETAIL_SUCCESS] (state, {treeItem, fileFields}) {
    state.files = {
      ...state.files,
      [ treeItem.id ]: {
        treeItem,
        fields: fileFields,
      }
    }
  },
  [ACTIVE_FILE_DETAIL] (state, id) {
    state.activeFileId = id
  },
  [UPDATE_FORM_FIELD] (state, {id, value}) {
    getActiveFileFields(state)
      .find(field => field.id == id)
      .value = value
  },
  [UPDATE_FORM_FILES] (state, {id, files}) {
    getActiveFileFields(state)
      .find(field => field.id == id)
      .files = Array.from(files)
  },
}

export default mutations
