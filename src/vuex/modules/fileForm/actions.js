import vue from 'vue'
import fileDetailsMapper from '@/util/FileDetailsMapper.js'
import { URL_BASE } from '@/config.js'
import {
  FETCH_FILE_DETAIL_SUCCESS,
  UPDATE_FORM_FIELD,
  UPDATE_FORM_FILES,
} from './mutation-type.js'
import {
  START_LOADING,
  STOP_LOADING
} from '@/vuex/modules/app/mutation-type.js'

/**
 * @param  {Function} options.commit  store
 * @param  {TreeItem} treeitem        selected item from select-tree
 * @return {Promise}                  Promise
 */
export const fetchFileDetails = ({commit}, { treeItem }) => {
  commit(START_LOADING)
  const promises = treeItem.formItems
    .map(uiElement => fetchFormElementDetail({commit}, uiElement))
  Promise.all(promises).then(fetchData => {
    const fileFields = fetchData.map(fileDetailsMapper)
    commit('ACTIVE_FILE_DETAIL', treeItem.id)
    commit('FETCH_FILE_DETAIL_SUCCESS', {treeItem, fileFields})
    commit(STOP_LOADING)
  }, reason => {
    console.error('reason',reason)
  });
}

/**
 * @param  {Function} options.commit            store
 * @param  {ShallowFormItem} shallowFormItem    shallow form item
 * @return {Promise}                            Promise
 */
const fetchFormElementDetail = ({commit}, shallowFormItem) => {
  commit(START_LOADING)
  const url = `${URL_BASE}/ui-elements/${shallowFormItem.name}.json`
  return vue.http.get(url)
    .then((res) => {
      commit(STOP_LOADING)
      if (res.status >= 200 && res.status < 300) {
        return res.data
      }
      return Promise.reject(new Error(res.status))
    })
}

/**
 * @param  {Function} options.commit            store
 * @return {Promise}                            Promise
 */
export const updateFormField = ({commit}, payload) => {
  commit(UPDATE_FORM_FIELD, payload)
}

/**
 * @param  {Function} options.commit            store
 * @return {Promise}                            Promise
 */
export const updateFormFiles = ({commit}, payload) => {
  commit(UPDATE_FORM_FILES, payload)
}

/**
 * @param  {Function} options.commit            store
 * @return {Promise}                            Promise
 */
export const postForm = ({commit, getters}) => {
  console.log("todo: postForm", getters.getPostData)
}
