export const getActiveFile = (state) => state.files[state.activeFileId]

export const getActiveFileFields = (state) => {
    const activeFile = getActiveFile(state)
    return activeFile ? activeFile.fields || [] : []
}
