import orderBy from 'lodash/orderBy'
import { getActiveFile, getActiveFileFields } from './util.js'

export const getFiles = state => state.files

export const getActiveFormFields = state => {
  return orderBy(getActiveFileFields(state), 'attributes.order', 'asc')
}

export const getActiveFormTitle = state => {
  const activeFile = getActiveFile(state)
  return activeFile && activeFile.treeItem && activeFile.treeItem.label
}

export const getPostData = state => {
  return getActiveFileFields(state).map(({name, value, id}) => ({name, value, id}))
}
