import cloneDeep from 'lodash/cloneDeep';
import {
  COMMIT_ROOT_TREEITEMS,
  COMMIT_CHILD_TREEITEM
} from './mutation-type'

import { updateSelectTree } from '@/util/SelectTreeUtil.js'

const mutations = {
  [COMMIT_ROOT_TREEITEMS] (state, treeItem) {
    state.selectTree = treeItem
  },
  [COMMIT_CHILD_TREEITEM] (state, {oldItem, newItem}) {
    console.log("COMMIT_CHILD_TREEITEM",{oldItem, newItem})
    const tree = cloneDeep(state.selectTree)
    updateSelectTree(tree, newItem)
    state.selectTree = tree
  },
}

export default mutations
