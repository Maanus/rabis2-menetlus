import {
  COMMIT_ROOT_TREEITEMS,
  COMMIT_CHILD_TREEITEM
} from './mutation-type.js'
import {
  START_LOADING,
  STOP_LOADING
} from '@/vuex/modules/app/mutation-type.js'
import { RootTreeItem, TreeItem, TreeItemDetail } from '@/model/TreeItem'
import Api from '@/resource'

/**
 * @param  {Function}        options.commit     store
 * @return {Promise}                            Promise
 */
export const fetchRootTreeItems = async ({commit}) => {
  try {
    commit(START_LOADING)
    let rootTreeItems = await Api.fetchRootTreeItems(commit)
    rootTreeItems = rootTreeItems.map(item => new RootTreeItem(item))
    rootTreeItems[1]._id = 10
    console.log(rootTreeItems)
    commit(COMMIT_ROOT_TREEITEMS, rootTreeItems)
    commit(STOP_LOADING)
  } catch(e) {
    commit(STOP_LOADING)
    console.error('Fetching root tree items failed', e)
  }
}

/**
 * @param  {Function} options.commit    store
 * @param  {TreeItem} treeItem          treeitem to which children are unknown 
 * @return {Promise}                    Promise
 */
export const fetchChildTreeItems = async ({commit}, {treeItem}) => {
  const emptyTreeItems = treeItem.children
  for (const treeItem of emptyTreeItems) {
    try {
      commit(START_LOADING)
      const res = await Api.fetchTreeItems(commit, treeItem)
      commit(COMMIT_CHILD_TREEITEM, {
        oldItem: treeItem,
        newItem: new TreeItemDetail(res.body)
      })
      commit(STOP_LOADING)
    } catch(e) {
      commit(STOP_LOADING)
      console.error('fetchChildTreeItems', e)
    }
  }
}
