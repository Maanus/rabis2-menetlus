import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import fileForm from './modules/fileForm'
import treeSelect from './modules/treeSelect'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    treeSelect,
    fileForm,
    app
  },
  strict: debug
})
