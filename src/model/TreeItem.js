import vue from 'vue'
import { URL_BASE } from '@/config.js'
import { ShallowFormItem } from './FormItem'
import mapper from '@/util/FormElementMapper.js'
import extractAttributes from '@/util/common.js'

class TreeUtil {
  static filterChildrenTreeItems(relatedTerms) {
    return relatedTerms
      .filter(relation => relation.RelationshipType === "NT")
      .map(relation => new TreeItem(relation))
  }
  static filterFormItems(relatedTerms) {
    return relatedTerms
      .filter(relation => relation.RelationshipType === "RT")
      .map(relation => new ShallowFormItem(relation))
  }
}


export class TreeItem {
  constructor(param) {
    this._id = param.Id
    this._name = param.Name
    if (param.RelatedTerms) {
      this._children = TreeUtil.filterChildrenTreeItems(param.RelatedTerms)
    }
  }

  get id() {
    return this._id
  }

  get name() {
    return this._name
  }

  get children() {
    return this._children || []
  }
}

export class RootTreeItem extends TreeItem {
  constructor(param) {
    super(param)
    if (param.Attributes)
      this._attributes = extractAttributes(param.Attributes)
  }

  get label() {
    return this._attributes.labelEst
  }
}

export class TreeItemDetail extends TreeItem {
  constructor(param) {
    super(param)
    if (param.Attributes)
      this._attributes = extractAttributes(param.Attributes)
    if (param.RelatedTerms) {
      this._formItems = TreeUtil.filterFormItems(param.RelatedTerms)
    }
  }

  get formItems() {
    return this._formItems || []
  }

  get label() {
    return this._attributes.labelEst
  }
}
