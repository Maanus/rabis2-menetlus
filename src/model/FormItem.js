export class FormItem {
  constructor({ id, name, attributes }) {
    this.id = id;
    this.name = name;
  }
}

export class ShallowFormItem extends FormItem {
  constructor({Id, Name}) {
    super({
      id: Id,
      name: Name
    })
  }
}

export class DetailFormItem extends FormItem {
  constructor({ id, name, attributes }) {
    super({id, name})
    this.value = null
    this.attributes = attributes
  }

  get label() {
    return this.attributes.labelEst
  }

  get type() {
    return this.attributes.type
  }
}

export class DetailFormFileItem extends DetailFormItem {
  constructor(params) {
    super(params)
    delete this.value
    this.files = []
  }
}
